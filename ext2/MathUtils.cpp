#include "MathUtils.h"
#define TAN_54 1.37638
#define FIVE 5
#define FOUR 4

#define THREE_TIMES_ROOT_THREE_DIVIDE_2 2.59807

double MathUtils::CalPentagonArea(double len)
{
	return (FIVE * len * len * TAN_54) / FOUR;
}

double MathUtils::CalHexagonArea(double len)
{
	return THREE_TIMES_ROOT_THREE_DIVIDE_2 * len * len;
}