#pragma once


class MathUtils
{
public:
	static double CalPentagonArea(double len);
	static double CalHexagonArea(double len);
};