#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"
#include "Hexagon.h"
#include "Pentagon.h"
#define SUM_ADJACENT_ANGLES 180
#define CHAR_INPUT_LEN 1

int main()
{
	std::string nam, col, tmpInput = ""; double rad = 0, ang = 0, ang2 = 0, len = 0; int height = 0, width = 0;
	Circle circ(nam, col, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Hexagon hex(nam, col, len);
	Pentagon pen(nam, col, len);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;

	Shape* ptrhex = &hex;
	Shape* ptrpen = &pen;
	
	std::cout << "Enter information for your objects" << std::endl;										//'p' is taken
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', hexagon = 'h' , pentagon = 'P'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		rad = 0, ang = 0, ang2 = 0, len = 0; //reseting fields for next shape
		height = 0, width = 0;

		std::cout << "\nwhich shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, P = pentagon" << std::endl;
		std::cin >> tmpInput; //firstly, I'm inputting to a temporary string

		
try
		{
	
	if (tmpInput.length() > CHAR_INPUT_LEN) //now, I'm checking if more than 1 char has been inputted
	{										
		std::cout << "Warning - Don't try to build more than one shape at once\n" << std::endl;
	}
		shapetype = tmpInput[0]; //using only the first character!
		
			switch (shapetype) {
		
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (rad == NULL)
				{
					throw InputException(); //if radius input is unvalid, throw an input exception
				}
				
				else if (rad < 0)
				{
					throw shapeException(); //if radius value is unvalid, throw an shape exception
				}
				
				
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (height == NULL || width == NULL)
				{
					throw InputException();
				}
				else if (height < 0 || width < 0) //not geometrically valid (length cannot be negative)
				{
					throw shapeException();
				}
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (width == NULL || height == NULL)
				{
					throw InputException();
				}
				else if (height < 0 || width < 0) //not geomtrically valid (length cannot be negative)
				{
					throw shapeException();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (height == NULL || width == NULL || (ang == NULL && ang2 == NULL))
				{                                     //^ when both of them are NULL -> input error (when one of them is NULL its fine [180 + 0 = 180])
					throw InputException();
				}
				else if (height <= 0 || width <= 0 || ang + ang2 != SUM_ADJACENT_ANGLES || ang < 0 || ang2 < 0) //not valid(length has to be positive + sum of adjacent angles has to be 180)
				{
					throw shapeException();
				}
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'P':
				std::cout << "enter name, color, len" << std::endl;
				std::cin >> nam >> col >> len;

				if (len == NULL)
				{
					throw InputException();
				}
				else if (len < 0)
				{
					throw shapeException();
				}
				pen.setName(nam);
				pen.setColor(col);
				pen.setLength(len);
				ptrpen->draw(); //it's hard for me to write this...
				break;
			case 'h':
				std::cout << "enter name, color, len" << std::endl;
				std::cin >> nam >> col >> len;

				if (len == NULL)
				{
					throw InputException();
				}
				else if (len < 0)
				{
					throw shapeException();
				}
				hex.setName(nam);
				hex.setColor(col);
				hex.setLength(len);
				ptrhex->draw(); 
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
		
		catch (const InputException& e)
		{

			std::cout << e.what() << std::endl;
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}

		catch (const std::exception& e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}

