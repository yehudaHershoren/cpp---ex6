#include <iostream>
#define ERROR 8200
#define ERROR_MESSAGE "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."

int add(int a, int b) {
	if (a + b == ERROR)
	{
		throw 0;
	}
 return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
	if (sum == ERROR)
	{
		throw 0;
	}
  };
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  return exponent;
}

int main(void) {

	
	
	try
	{
		std::cout <<  add(8000, 200) << "\n" << std::endl; //invalid
		
	}
	catch (int a)
	{
		std::cerr << ERROR_MESSAGE << std::endl;
	}
	

	try
	{
		std::cout << multiply(100, 100) << "\n" << std::endl; //invalid
	}
	catch (int a)
	{
		std::cerr << ERROR_MESSAGE << std::endl;
	}
	

	try
	{
		std::cout << pow(100, 10) << "\n" << std::endl; //invalid
	}
	catch (int a)
	{
		std::cerr << ERROR_MESSAGE << std::endl;
	}


	try
	{
		std::cout << pow(2, 20) << "\n" << std::endl; //valid
	}
	catch (int a)
	{
		std::cerr << ERROR_MESSAGE << std::endl;
	}

	

	getchar();
}